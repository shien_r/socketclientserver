package test06;

public class Main {
 	/*
 	 * 1. クライアント <- サーバ
 	 * 2. サーバはメッセージを受け取るクライアントを待ち続ける
 	 * 
 	 * サーバがクライアントの接続を受け付ける回数は10回まで
 	 * 10回受け取ったら終わるが、生成されるクライアントは3つ
 	 * なのでサーバはずっと回り続ける
	 */
	public static void main(String[] args) {
		System.out.println("始まるよ");
		Thread serverThread = new Thread(new SocketServer());
		Thread clientThread1 = new Thread(new SocketClient());
		Thread clientThread2 = new Thread(new SocketClient());
		Thread clientThread3 = new Thread(new SocketClient());
		
		serverThread.start();
		clientThread1.start();
		clientThread2.start();
		clientThread3.start();

		//終了するのを待つ
		try {
			clientThread1.join();// 先に終わるのはクライアント側
			clientThread2.join();
			clientThread3.join();
			serverThread.join(); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 

		System.out.println("全てのスレッドが終了しました");
	}
}
