package test06;

import java.io.*;
import java.net.*;

public class SocketServer implements Runnable {
	@Override
	public void run() {
		int port = 12345;

		try {
			System.out.println("サーバー「待ってる」");
			ServerSocket ss = new ServerSocket(port);	

			int count = 0;
			while (count < 10) { // 10回接続して返すまで終わらない
				Socket s = ss.accept(); // ソケットを開く

				//メッセージを返す
				OutputStream os = s.getOutputStream();
				DataOutputStream dos = new DataOutputStream(os);
				dos.writeUTF("Hello World");
				System.out.println("サーバ「" + count++ + "回 メッセージを返した」");

				os.close();			
				s.close();			
				dos.close();
			}
			ss.close();


			System.out.println("close server");

		} catch (Exception e) {
			System.out.println("Exception: " + e + " server が死んだ");
		}

	}
}
