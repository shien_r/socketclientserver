package test04;
import java.io.*;
import java.net.*;
public class SocketClient implements Runnable {
	@Override
	public void run() {
        try {
            String address = "127.0.0.1";
            int port = 12348;
            Socket s = new Socket(address, port);//ソケットを開いて接続
            
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
            System.out.println("クライアント「送るよ」");
            dos.writeUTF("HelloWorld by client"); //送信
            System.out.println("クライアント「送った」");            
            
            
            InputStream is = s.getInputStream();                        
            DataInputStream dis = new DataInputStream(is);    
            System.out.println("クライアント「受け取る」");
            String message = dis.readUTF();
            System.out.println("クライアント「" + message + " だと」");
            
            // DataInputStream が内部的に、受け取った InputStream を閉じるから
            // 送り返す前に閉じてしまうとエラーになるので最後にまとめて閉じる　
            dos.close();
            dis.close();
            is.close();
            s.close();
            System.out.println("close client");
            
        } catch (Exception e) {
            System.out.println("Exception: " + e + " clientが死んだ");
        }
	}
}