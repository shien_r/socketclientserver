package test05;

import java.io.*;
import java.net.*;

public class SocketServer implements Runnable {
	@Override
	public void run() {
		int port = 12348;

		try {
			System.out.println("サーバー「待ってる」");
			ServerSocket ss = new ServerSocket(port);	


			Socket s = ss.accept(); // ソケットを開く

			//メッセージを受け取る
			InputStream is = s.getInputStream();
			DataInputStream dis = new DataInputStream(is);	
			String message = dis.readUTF(); // 受信する(何か受信するまでとまる)
			System.out.println(message);
			System.out.println("サーバ「受け取った」");
			
			//メッセージを返す
			OutputStream os = s.getOutputStream();
			DataOutputStream dos = new DataOutputStream(os);
			dos.writeUTF("「食べた」");
			System.out.println("サーバ「メッセージを返した」");
			
			dis.close();
			dos.close();
			is.close();
			os.close();

			s.close();
			ss.close();


			System.out.println("close server");

		} catch (Exception e) {
			System.out.println("Exception: " + e + " server が死んだ");
		}

	}
}
