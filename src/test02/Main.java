package test02;

public class Main {
 	/*
 	 * 1. クライアント -> サーバ
 	 * 2. クライアント -> サーバ
 	 * 3. クライアント -> サーバ
	 */
	public static void main(String[] args) {
		int id = 0;
		System.out.println("始まるよ");
		Thread serverThread = new Thread(new SocketServer());
		Thread clientThread1 = new Thread(new SocketClient(id++));
		Thread clientThread2 = new Thread(new SocketClient(id++));
		Thread clientThread3 = new Thread(new SocketClient(id++));
		
		serverThread.start();
		clientThread1.start();// 1
		clientThread2.start();// 2
		clientThread3.start();// 3 

		//終了するのを待つ
		try {
			clientThread1.join();// 先に終わるのはクライアント側
			clientThread2.join();// 先に終わるのはクライアント側
			clientThread3.join();// 先に終わるのはクライアント側
			serverThread.join(); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 

		System.out.println("全てのスレッドが終了しました");
	}
}
