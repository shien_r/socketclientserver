package test02;

import java.io.*;
import java.net.*;

public class SocketServer implements Runnable {
	@Override
	public void run() {
		int port = 12348;
		int count = 0;
		try {
			System.out.println("サーバー「待ってる」");
			ServerSocket ss = new ServerSocket(port);	

			while (count < 3) {

				Socket s = ss.accept(); // ソケットを開く
				InputStream is = s.getInputStream();
				DataInputStream dis = new DataInputStream(is);	
				String message = dis.readUTF(); // 受信する(何か受信するまでとまる)
				System.out.println(message);
				System.out.println("サーバ「 " + count + "回 受け取った」");
				count++;
				dis.close();
				is.close();
				s.close();
			}
			ss.close();
			System.out.println("close server");

		} catch (Exception e) {
			System.out.println("Exception: " + e + " server が死んだ");
		}

	}
}
