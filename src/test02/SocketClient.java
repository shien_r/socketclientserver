package test02;
import java.io.*;
import java.net.*;
public class SocketClient implements Runnable {
	private final int id;
	
	SocketClient(int _id) {
		this.id = _id;
	}
	
	@Override
	public void run() {
        try {
            String server = "127.0.0.1";
            int port = 12348;
            Socket s = new Socket(server, port);//ソケットを開いて接続
            
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
            System.out.println("クライアント" + id + "「送るよ」");
            dos.writeUTF("HelloWorld by client " + id); //送信
            System.out.println("クライアント" + id + "「送った」");
            dos.close();
            s.close();
            System.out.println("close client" + id);
        } catch (Exception e) {
            System.out.println("Exception: " + e + " client" + id + "が死んだ");
        }
	}
}