package test03;

public class Main {
 	/*
 	 * 1. クライアント -> サーバ つなぐだけ
 	 * 2. クライアント <- サーバ message
 	 * 3. クライアント message 出力
	 */
	public static void main(String[] args) {
		System.out.println("始まるよ");
		Thread serverThread = new Thread(new SocketServer());
		Thread clientThread = new Thread(new SocketClient());
		
		serverThread.start();
		clientThread.start();

		//終了するのを待つ
		try {
			clientThread.join();// 先に終わるのはクライアント側
			serverThread.join(); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 

		System.out.println("全てのスレッドが終了しました");
	}
}
