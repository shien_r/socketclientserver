package test03;
import java.io.*;
import java.net.*;
public class SocketClient implements Runnable {
	@Override
	public void run() {
        try {
            String address = "127.0.0.1";
            int port = 12348;
            Socket s = new Socket(address, port);//ソケットを開いて接続
            
            DataInputStream dis = new DataInputStream(s.getInputStream());
            System.out.println("クライアント「受け取るよ」");
            String message = dis.readUTF();
            System.out.println("クライアント「" + message + "受け取った」");
            dis.close();
            s.close();
            System.out.println("close client");
        } catch (Exception e) {
            System.out.println("Exception: " + e + " client が死んだ");
        }
	}
}