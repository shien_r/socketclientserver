package test03;

import java.io.*;
import java.net.*;

public class SocketServer implements Runnable {
	@Override
	public void run() {
		int port = 12348;
		try {
			System.out.println("サーバー「待ってる」");
			ServerSocket ss = new ServerSocket(port);	

			Socket s = ss.accept(); // ソケットを開く
			OutputStream os = s.getOutputStream();
			DataOutputStream dos = new DataOutputStream(os);
			System.out.println("サーバ「送る」");
			dos.writeUTF("Hello World"); // 受信する(何か受信するまでとまる)
			System.out.println("サーバ「送った」");
			dos.close();
			os.close();
			s.close();
			ss.close();
			System.out.println("close server");

		} catch (Exception e) {
			System.out.println("Exception: " + e + " server が死んだ");
		}

	}
}
