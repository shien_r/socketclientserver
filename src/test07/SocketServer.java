package test07;

import java.io.*;
import java.net.*;

public class SocketServer implements Runnable {
	@Override
	public void run() {
		int port = 12345;

		try {
			System.out.println("サーバー「待ってる」");
			ServerSocket ss = new ServerSocket(port);	

			int count = 0;
			while (count < 3) {
				Socket s = ss.accept(); // ソケットを開く
				System.out.println(Thread.currentThread());
				//メッセージを返す
				OutputStream os = s.getOutputStream();
				DataOutputStream dos = new DataOutputStream(os);
				
				//無限ループで待つ　writeUTF にたどり着かないためクラアントが待機する 
				//int waitTime = 0;
				//while(waitTime < 100) {}
				
				//5分くらいの重い処理　writeUTF にたどり着かないため、クライアントは待機する
				//終わったら、クライアントにメッセージを返す
				//ちゃんと全てのクライアントへ送信できた
				int primeCount = 0;
				for (int i=1; i < 10000000; i++) {
					for (int j=1; j < Math.sqrt(i); j++) {
						if(i % j == 0) {
							primeCount++;							
						}
					}
				}

				dos.writeUTF(primeCount + " Hello, World\n");//メッセージの送信
				
				//無限ループでまつ　送信した一つ目は受け取れる。そのあとのクライアント2つは待機。　
				//int waitTime = 0;
				//while(waitTime < 100) {}
				
				System.out.println("サーバ「" + count++ + "回 メッセージを返した」");

				os.close();			
				s.close();			
				dos.close();
			}
			ss.close();
			
			System.out.println("close server");
		} catch (Exception e) {
			System.out.println("Exception: " + e + " server が死んだ");
		}

	}
}
