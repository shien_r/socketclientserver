package test07;

public class Main {
 	/*
 	 * 1. クライアント <- サーバ
 	 * 
 	 *  サーバが長時間処理をしていて、その間に他のクライアントからの接続要求が来たら
 	 *  どうなるかの実験
 	 *  
	 */
	public static void main(String[] args) {
		System.out.println("始まるよ");
		Thread serverThread = new Thread(new SocketServer());
		Thread clientThread1 = new Thread(new SocketClient());
		Thread clientThread2 = new Thread(new SocketClient());
		Thread clientThread3 = new Thread(new SocketClient());

		serverThread.start();
		clientThread1.start();
		clientThread2.start();
		clientThread3.start();

		//終了するのを待つ
		try {
			clientThread1.join();// 先に終わるのはクライアント側
			clientThread2.join();
			clientThread3.join();
			serverThread.join(); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 

		System.out.println("全てのスレッドが終了しました");
	}
}
