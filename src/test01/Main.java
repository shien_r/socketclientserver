package test01;

public class Main {
	/*
	 * クライアント:送る
	 * サーバ：受け取る
	 */
	public static void main(String[] args) {
		System.out.println("始まるよ");
		Thread serverThread = new Thread(new SocketServer());
		Thread clientThread = new Thread(new SocketClient());
		
		serverThread.start();
		clientThread.start();

		//終了するのを待つ
		try {
			clientThread.join();// 先に終わるのはクライアント側
			serverThread.join(); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 

		System.out.println("全てのスレッドが終了しました");

	}
}
