package test01;
import java.io.*;
import java.net.*;
public class SocketClient implements Runnable {
	@Override
	public void run() {
        try {
            String address = "127.0.0.1";
            int port = 12348;
            Socket s = new Socket(address, port);//ソケットを開いて接続
            
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
            System.out.println("クライアント「送るよ」");
            dos.writeUTF("HelloWorld"); //送信
            System.out.println("クライアント「送った」");
            dos.close();
            s.close();
            System.out.println("close client");
        } catch (Exception e) {
            System.out.println("Exception: " + e + " client");
        }
	}
}