package test01;

import java.io.*;
import java.net.*;

public class SocketServer implements Runnable {
	@Override
	public void run() {
		try {
			int port = 12348;
			ServerSocket ss = new ServerSocket(port);
			Socket s = ss.accept(); // ソケットを開く
			System.out.println("サーバー「待ってる」");
			InputStream is = s.getInputStream();
			
			DataInputStream dis = new DataInputStream(is);	
			String message = dis.readUTF(); // 受信する(何か受信するまでとまる)
			System.out.println(message);
			System.out.println("サーバ「受け取った」");
			
			dis.close();
			s.close();
			ss.close();
			System.out.println("close server");
			
		} catch (Exception e) {
			System.out.println("Exception: " + e + " server");
		}
	}
}
